import React from 'react'
import Head from 'next/head'
import Noticias from '../components/noticias';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { Button, Container, Card, CardContent, CardHeader, CardMedia, Typography, Badge, Link } from '@mui/material';


const Index = (props) => {
  return (
    <div>
      <Head>
        <title>Portal de Noticia "Hoción"</title>
      </Head>
      <Container maxWidth="sm">
        <div className="jumbotron">
          <h1 className="display-4">Portal de Noticia "Hoción"</h1>
          <p className="lead">Este es un portal de noticias creado con Next.js y MUI</p>
        </div>
        <Noticias noticias={props.noticias}/>
      </Container>
    </div>
    )
}

Index.getInitialProps = async (ctx) => {
  const res = await fetch('http://localhost:3333/noticias')
  const data = await res.json()
  return { noticias: data }
}

export default Index