#FRONTED

# Portal de noticias

# EL HOCICÓN

## Tecnologías utilizadas

- NodeJS versión 20 (https://nodejs.org/)
- Framework: NextJS versión 14 (https://nextjs.org)
- CSS: MUI(https://mui.com)

## Instalación

Descargar del Repositorio

```bash
$ git clone https://gitlab.com/carlosramos14/noticias-hocicon-frontend.git
```

Ir a la carpeta del proyecto descargada

```bash
$ cd noticias-hocicon-frontend/
```

Instalar el nestjs y sus dependencias

```bash
$ npm install
```

## 

## Correr la aplicación 

Ir a la raiz del proyecto y ejecutar

```bash
$ npm run dev
```

Por defecto se levantará en el **puerto 3000**



Abrir un navegador y entrar a http://localhost:3333