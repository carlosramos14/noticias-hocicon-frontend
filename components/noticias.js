import { Button, Container, Card, CardContent, CardHeader, CardMedia, Typography, Badge, Link } from '@mui/material';
import NoticiaCard from './noticiaCard';


const SeccionNoticias = (props) => {
  return (
    <div>
      {props.noticias.map((noticia) => (
        <NoticiaCard noticia={noticia}/>
        ))
      }
    </div>
  );
}

export default SeccionNoticias;