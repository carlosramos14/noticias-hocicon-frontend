import { Button, Container, Card, CardContent, CardHeader, CardMedia, Typography, Badge, Link } from '@mui/material';


const NoticiaCard = (props) => {
  return (
        <Card>
          <CardHeader
            title={props.noticia.titulo}
            subheader={`${props.noticia.fechaPublicacion}`}
          />
          <CardMedia
            component="img"
            height="160"
            image="https://picsum.photos/200/160"
            alt={props.noticia.titulo}
          />
          <CardContent>
            <Typography variant="body2" color="text.secondary">
              {props.noticia.contenido}
            </Typography>
            <br/>
            <Badge color="secondary" variant="filled" overlap="rectangular">
              Lugar: {props.noticia.lugar}
            </Badge>
          </CardContent>
          <CardContent>
            <Button >
              Ver más
            </Button>
          </CardContent>
        </Card>
  );
}

export default NoticiaCard;